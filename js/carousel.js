$(document).ready(function(){
	$('.carousel').carousel({
		'interval': 2000,
		'wrap': false
	});
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').click(function(e){
		e.preventDefault();
		$(this).popover();
	});

})