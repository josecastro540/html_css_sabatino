$(document).ready(function(){
	var body = $('body');
  	var color= $('#color');
  	var nombre = $('#nombre');

	 color.change(function(){
    	changeBg();
 	 });

	function changeBg(){
	    body.css({
	      'background-color': color.val()
		})
  	}

  	function validar(){
  		if(nombre.val()=="" || nombre.val==null){
  			$('#error').text('* El nombre es obligatorio')
  		}else{
  			$('#error').text('');
  		}
  	}

  	$('.btn').click(function(){
  		validar();
  	})
});